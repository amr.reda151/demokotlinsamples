package com.amr.demokotlin

import com.amr.demokotlin.tdd.ex1.LogicContracts
import com.amr.demokotlin.tdd.ex1.Presenter
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.never
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class TestTddActivityPresenterLogic {
    lateinit var presenter: LogicContracts.Presenter

    @Mock
    lateinit var view: LogicContracts.View

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        presenter = Presenter(view)
    }

    @Test
    fun sendEmptyMessage() {
        presenter.sendMessage("")
        verify(view, never()).addMessageToScreen("")
    }

    @Test
    fun sendNormalMessage() {
        presenter.sendMessage("amr")
        verify(view).addMessageToScreen("amr")
    }

    @Test
    fun messageInMessageField() {
        presenter.messageFieldChanged("amr")
        verify(view).enableSendButton()
    }

    @Test
    fun emptyMessageInMessageField() {
        presenter.messageFieldChanged("")
        verify(view).disableSendButton()
    }
}