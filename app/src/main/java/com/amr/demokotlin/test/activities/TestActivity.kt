package com.amr.demokotlin.test.activities

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.amr.demokotlin.R
import com.amr.demokotlin.utilities.managers.RetrofitClientManager
import com.amr.demokotlin.utilities.managers.RuntimePermissionsManager
import com.amr.demokotlin.utilities.managers.RuntimePermissionsManagerInterface
import com.amr.demokotlin.utilities.managers.location.*
import kotlinx.android.synthetic.main.activity_test.*

enum class LanguageCode(val code: String) {
    EN("en"), AR("ar")
}

class TestActivity : AppCompatActivity() {

    private lateinit var runtimePermissionsManager: RuntimePermissionsManagerInterface
    private lateinit var fullAddressManager: FullAddressManagerInterface
    private lateinit var userLocationManager: UserLocationManagerInterface
    private lateinit var locationManager: LocationManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)

        runtimePermissionsManager = RuntimePermissionsManager(this)
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        testAddressManager()

        testLocationManager(true)
    }

    private fun testLocationManager(getFullAddress: Boolean) {
        userLocationManager = UserLocationManager(
            this,
            true
        )

        userLocationManager.latLong.observe(this, Observer {
            location.text = "${it.latitude}, ${it.longitude}"

            if (getFullAddress) {
                Toast.makeText(this@TestActivity, R.string.getting_full_address, Toast.LENGTH_SHORT).show()

                fullAddressManager.getFullAddressOfLocation(it.latitude, it.longitude)

                fullAddressManager.fullAddress.observe(this, Observer { fullAddress: String ->
                    address.text = fullAddress
                })
            }
        })
    }

    private fun testAddressManager() {
        fullAddressManager = FullAddressManager(
            this,
            RetrofitClientManager(),
            LanguageCode.EN
        )
    }

    override fun onResume() {
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (runtimePermissionsManager.areAllPermissionsGranted(
                    arrayOf(
                        android.Manifest.permission.ACCESS_FINE_LOCATION,
                        android.Manifest.permission.ACCESS_COARSE_LOCATION
                    )
                )
            ) {
                userLocationManager.getCurrentLocation()
            } else {
                runtimePermissionsManager.showRequestPermissionsDialog(
                    arrayOf(
                        android.Manifest.permission.ACCESS_FINE_LOCATION,
                        android.Manifest.permission.ACCESS_COARSE_LOCATION
                    ),
                    1
                )
            }
        } else {
            showGpsDisabledAlert()
        }

        super.onResume()
    }

    private fun showGpsDisabledAlert() {
        AlertDialog.Builder(this)
            .setMessage(resources.getString(R.string.gps_is_not_enabled))
            .setCancelable(false)
            .setPositiveButton(resources.getString(R.string.yes)) { dialog, _ ->
                dialog.dismiss()

                val gpsSettingIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(gpsSettingIntent)
            }
            .setNegativeButton(resources.getString(R.string.cancel)) { dialog, id ->
                dialog.dismiss()
            }
            .create()
            .show()
    }

//    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
//
//        if (runtimePermissionsManager.checkRequestPermissionsResult(grantResults, 1, requestCode)) {
////            fullAddressManager.getCurrentFullAddress()
//            locationManager.getCurrentLocation()
//        }
//
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
//    }
}
