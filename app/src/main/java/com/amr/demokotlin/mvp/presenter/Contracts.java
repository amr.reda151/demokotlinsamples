package com.example.amr.demoapp.mvp.presenter;

public class Contracts {

    public interface IMainView {
        public void changeData(String newData);
    }

    public interface IMainPresenter {
        public void requestChangeData();
    }

    public interface IMainModel {
        public String getData();
    }

}
