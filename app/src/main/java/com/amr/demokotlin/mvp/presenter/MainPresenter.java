package com.amr.demokotlin.mvp.presenter;


import com.amr.demokotlin.mvp.model.MainModel;

public class MainPresenter implements com.example.amr.demoapp.mvp.presenter.Contracts.IMainPresenter {

    private MainModel mainModel;
    private com.example.amr.demoapp.mvp.presenter.Contracts.IMainView iMainView;

    public MainPresenter(com.example.amr.demoapp.mvp.presenter.Contracts.IMainView iMainView) {
        mainModel = new MainModel();
        this.iMainView = iMainView;
    }

    @Override
    public void requestChangeData() {
        iMainView.changeData(mainModel.getData());
    }
}
