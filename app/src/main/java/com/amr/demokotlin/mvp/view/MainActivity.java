package com.amr.demokotlin.mvp.view;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import com.amr.demokotlin.R;
import com.amr.demokotlin.mvp.presenter.MainPresenter;
import com.example.amr.demoapp.mvp.presenter.Contracts;

public class MainActivity extends AppCompatActivity implements Contracts.IMainView {

    private MainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mvp);

        mainPresenter = new MainPresenter(this);
    }

    public void changeData(View view) {
        mainPresenter.requestChangeData();
    }

    @Override
    public void changeData(String newData) {
        TextView status = findViewById(R.id.status);
        status.setText(newData);
    }
}
