package com.amr.demokotlin.tdd.ex1

class LogicContracts {
    interface View {
        fun addMessageToScreen(Message: String)

        fun enableSendButton()

        fun disableSendButton()
    }

    interface Presenter {
        fun sendMessage(Message: String)

        fun messageFieldChanged(Message: String)
    }
}