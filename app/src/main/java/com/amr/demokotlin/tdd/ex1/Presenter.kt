package com.amr.demokotlin.tdd.ex1

class Presenter(val view: LogicContracts.View) :
    LogicContracts.Presenter {


    override fun sendMessage(Message: String) {
        if (!Message.isEmpty()) {
            view.addMessageToScreen(Message)
        }
    }

    override fun messageFieldChanged(Message: String) {
        if (Message.isEmpty()) {
            view.disableSendButton()
        } else {
            view.enableSendButton()
        }
    }
}