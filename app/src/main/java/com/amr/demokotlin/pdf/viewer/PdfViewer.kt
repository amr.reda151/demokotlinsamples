package com.amr.demokotlin.pdf.viewer

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_pdf_viewer.*


class PdfViewer : AppCompatActivity() {

    companion object {
        private var pageIndex: Int = 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.amr.demokotlin.R.layout.activity_pdf_viewer)

        displaySpecificSectionsOnlyInPdf()

        zoomIn.setOnClickListener {
            pdfView.zoomWithAnimation(5f)
        }

        zoomOut.setOnClickListener {
            pdfView.zoomWithAnimation(1f)
        }
    }

    private fun startPdfFileAt(index: Int) {
        pdfView.fromAsset("pdf_test.pdf")
            .enableSwipe(true) // allows to block changing pages using swipe
            .swipeHorizontal(false)
            .enableDoubletap(true)
            .defaultPage(index)
            // allows to draw something on the current page, usually visible in the middle of the screen
            .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
            .enableAntialiasing(true) // improve rendering a little bit on low-res screens
            .spacing(1) // spacing between pages in dp. To define spacing color, set view background
            .onPageChange { page, pageCount ->
                Toast.makeText(this@PdfViewer, page.toString(), Toast.LENGTH_SHORT).show()
            }
            .load()

        moveTo.setOnClickListener {
            pageIndex += 10
            startActivity(Intent(this@PdfViewer, PdfViewer::class.java))
            finish()
        }
    }

    private fun displaySpecificSectionsOnlyInPdf() {
        val pagesRange = getPagesRange(0, 9)

        pdfView.fromAsset("pdf_test.pdf")
            .enableSwipe(true) // allows to block changing pages using swipe
            .swipeHorizontal(false)
            .enableDoubletap(true)
            .pages(*pagesRange)
            // allows to draw something on the current page, usually visible in the middle of the screen
            .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
            .enableAntialiasing(true) // improve rendering a little bit on low-res screens
            .spacing(1) // spacing between pages in dp. To define spacing color, set view background
            .onPageChange { page, pageCount ->
                Toast.makeText(this@PdfViewer, page.toString(), Toast.LENGTH_SHORT).show()
            }
            .load()
    }

    private fun getPagesRange(startIndex: Int, endIndex: Int): IntArray {
        val pagesSize = endIndex - startIndex

        val pages = IntArray(pagesSize)
        var tmpStartIndex = startIndex

        for(i in 0 until pagesSize) {
            pages[i] = tmpStartIndex
            tmpStartIndex += 1
        }

        return pages
    }
}
