package com.amr.demokotlin.utilities.extensions.arraylist

fun <T> List<T>.toArrayList(): ArrayList<T> {
    return ArrayList(this)
}