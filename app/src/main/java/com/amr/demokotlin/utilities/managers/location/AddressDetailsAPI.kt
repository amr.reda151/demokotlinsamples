package com.amr.demokotlin.utilities.managers.location

import com.amr.demokotlin.utilities.managers.location.data.classes.Address
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface AddressDetailsAPI {
    @GET("geocode/json")
    fun getAddressDetailsAsync(
        @Query("latlng") latLng: String,
        @Query("language") language: String,
        @Query("key") key: String
    ): Deferred<Response<Address>>
}