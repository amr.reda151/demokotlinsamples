package com.amr.demokotlin.utilities;


import android.app.Application;
import android.content.Context;
import com.amr.demokotlin.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class MainApp extends Application {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("Fonts/DIN Next LT W23 Regular.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

}
