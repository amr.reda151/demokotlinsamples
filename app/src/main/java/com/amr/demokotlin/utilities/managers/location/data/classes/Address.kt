package com.amr.demokotlin.utilities.managers.location.data.classes

import com.google.gson.annotations.SerializedName

data class Address(
    @SerializedName("plus_code")
    var plusCode: PlusCode = PlusCode(),
    var results: List<Result> = listOf(),
    var status: String = ""
) {
    data class Result(
        @SerializedName("address_components")
        var addressComponents: List<AddressComponent> = listOf(),
        @SerializedName("formatted_address")
        var formattedAddress: String = "",
        var geometry: Geometry = Geometry(),
        @SerializedName("place_id")
        var placeId: String = "",
        var types: List<String> = listOf()
    ) {
        data class AddressComponent(
            @SerializedName("long_name")
            var longName: String = "",
            @SerializedName("short_name")
            var shortName: String = "",
            var types: List<String> = listOf()
        )

        data class Geometry(
            var bounds: Bounds = Bounds(),
            var location: Location = Location(),
            @SerializedName("location_type")
            var locationType: String = "",
            var viewport: Viewport = Viewport()
        ) {
            data class Viewport(
                var northeast: Northeast = Northeast(),
                var southwest: Southwest = Southwest()
            ) {
                data class Southwest(
                    var lat: Double = 0.0,
                    var lng: Double = 0.0
                )

                data class Northeast(
                    var lat: Double = 0.0,
                    var lng: Double = 0.0
                )
            }

            data class Location(
                var lat: Double = 0.0,
                var lng: Double = 0.0
            )

            data class Bounds(
                var northeast: Northeast = Northeast(),
                var southwest: Southwest = Southwest()
            ) {
                data class Southwest(
                    var lat: Double = 0.0,
                    var lng: Double = 0.0
                )

                data class Northeast(
                    var lat: Double = 0.0,
                    var lng: Double = 0.0
                )
            }
        }
    }

    data class PlusCode(
        @SerializedName("compound_code")
        var compoundCode: String = "",
        @SerializedName("global_code")
        var globalCode: String = ""
    )
}