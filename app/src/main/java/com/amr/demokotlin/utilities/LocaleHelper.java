package com.amr.demokotlin.utilities;

import android.content.Context;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

import java.util.Locale;


public class LocaleHelper extends CalligraphyContextWrapper {

    private static final String SELECTED_LANGUAGE = "AppliedLanguage";

    /**
     * Override the default AttributeId, this will always take the custom attribute defined here
     * and ignore the one set in {@link CalligraphyConfig}.
     * <p>
     * Remember if you are defining default in the
     * {@link CalligraphyConfig} make sure this is initialised before
     * the activity is created.
     *
     * @param base        ContextBase to Wrap
     * @param attributeId Attribute to lookup.
     * @deprecated use {@link #wrap(Context)}
     */
    @SuppressWarnings("deprecation")
    public LocaleHelper(Context base, int attributeId) {
        super(base, attributeId);
    }

    @NonNull
    public static Context onAttach(Context context) {
        return onAttach(context, Locale.getDefault().getLanguage());
    }

    @NonNull
    private static Context onAttach(Context context, String defaultLanguage) {
        String language = getSelectedLanguage(context, defaultLanguage);
        return setLocale(context, language);
    }

    @NonNull
    public static Context setLocale(Context context, String language) {
        saveSelectedLanguage(context, language);
        return updateResources(context, language);
    }

    @NonNull
    public static String getLanguage(Context context) {
        return getSelectedLanguage(context, Locale.getDefault().getLanguage());
    }

    public static void saveSelectedLanguage(Context context, String language) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit().putString(SELECTED_LANGUAGE, language).apply();
    }

    @NonNull
    private static String getSelectedLanguage(Context context, String defaultLanguage) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(SELECTED_LANGUAGE, defaultLanguage);
    }

    @NonNull
    private static Context updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Configuration configuration = context.getResources().getConfiguration();
        configuration.setLocale(locale);

        return CalligraphyContextWrapper.wrap(context.createConfigurationContext(configuration));
    }

}