package com.amr.demokotlin.utilities.enums

enum class LanguageCode(val code: String) {
    EN("en"), AR("ar")
}