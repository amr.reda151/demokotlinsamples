package com.amr.demokotlin.utilities.extensions.recyclerview

import androidx.recyclerview.widget.RecyclerView

fun RecyclerView.setup(block: (RecyclerView) -> Unit) {
    block(this)
}