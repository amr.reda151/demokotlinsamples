package com.amr.demokotlin.utilities.managers.location

import android.content.Context
import android.location.Geocoder
import androidx.lifecycle.MutableLiveData
import com.amr.demokotlin.utilities.managers.RetrofitClientManagerInterface
import com.amr.demokotlin.BuildConfig
import com.amr.demokotlin.R
import com.amr.demokotlin.utilities.managers.location.data.classes.Address
import com.amr.demokotlin.test.activities.LanguageCode
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response
import java.util.*

interface FullAddressManagerInterface {
    var fullAddress: MutableLiveData<String>

    fun getFullAddressOfLocation(latitude: Double, longitude: Double)
}

class FullAddressManager(
    private val context: Context,
    private val retrofitClientManager: RetrofitClientManagerInterface,
    private val languageCode: LanguageCode
) : FullAddressManagerInterface {

    override var fullAddress = MutableLiveData<String>()

    private val locale = Locale(languageCode.code)
    private var geocoder = Geocoder(context, locale)

    override fun getFullAddressOfLocation(latitude: Double, longitude: Double) {
        CoroutineScope(Dispatchers.IO).launch {
            val fullAddress = getAddressUsingGeocoder(latitude, longitude)
            fullAddress?.let {
                val result = getAddressUsingGoogleApis(latitude, longitude)

                if (result.body()?.results?.isNotEmpty() == true) {
                    withContext(Dispatchers.Main) {
                        this@FullAddressManager.fullAddress.value = result.body()!!.results.first().formattedAddress
                    }
                } else {
                    withContext(Dispatchers.Main) {
                        this@FullAddressManager.fullAddress.value =
                            context.resources.getString(R.string.no_address_found)
                    }
                }
            } ?: run {
                withContext(Dispatchers.Main) {
                    this@FullAddressManager.fullAddress.value = fullAddress
                }
            }
        }
    }

    private fun getAddressUsingGeocoder(latitude: Double, longitude: Double): String? {
        val firstAddress = geocoder.getFromLocation(latitude, longitude, 1)
            .filterNotNull()
            .firstOrNull()

        firstAddress?.let {
            return arrayOf(
                it.countryName,
                it.adminArea,
                it.subAdminArea,
                it.locality,
                it.featureName
            )
                .filterNotNull()
                .filter { string ->
                    string != "Unnamed Road"
                }
                .joinToString(", ")
        }

        return null
    }

    private suspend fun getAddressUsingGoogleApis(latitude: Double, longitude: Double): Response<Address> {
        val api = retrofitClientManager.client.create(AddressDetailsAPI::class.java)

        return api.getAddressDetailsAsync(
            "$latitude,$longitude",
            languageCode.code,
            BuildConfig.GEOCODING_API_KEY
        ).await()
    }
}