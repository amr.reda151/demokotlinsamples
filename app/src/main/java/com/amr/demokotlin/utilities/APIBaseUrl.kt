package com.amr.demokotlin.utilities

class APIBaseUrl {
    companion object {
        private const val BASE_URL = "http://aghzaty.dev.ibtdi.work/"
        const val API_URL = BASE_URL + "api/"

        const val GOOGLE_MAPS_API_URL = "https://maps.googleapis.com/maps/api/"
    }
}