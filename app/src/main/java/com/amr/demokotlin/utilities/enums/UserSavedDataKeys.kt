package com.amr.demokotlin.utilities.enums

enum class UserSavedDataKeys(val key: String) {
    ID("id"), USERNAME("username"), EMAIL("email"),
    PHONE("phone"), ADDRESS_ID("addressId"), TOKEN("token"), ROLE("role")
}