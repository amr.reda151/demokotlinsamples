package com.amr.demokotlin.wheel.task

import android.annotation.SuppressLint
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.amr.demokotlin.R
import com.bluehomestudio.luckywheel.WheelItem
import kotlinx.android.synthetic.main.activity_wheel.*
import kotlinx.android.synthetic.main.activity_wheel.view.*

class WheelActivity : AppCompatActivity() {

    @SuppressLint("ResourceAsColor")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wheel)

        wheel1()
    }

    fun wheel1() {
        val wheelItems = ArrayList<WheelItem>()
        wheelItems.add(WheelItem(Color.LTGRAY, BitmapFactory.decodeResource(resources, R.drawable.baseline_tag_faces_white_48)))

        wheelItems.add(WheelItem(Color.GRAY, BitmapFactory.decodeResource(resources, R.drawable.baseline_tag_faces_white_48)))

        wheelItems.add(WheelItem(Color.LTGRAY, BitmapFactory.decodeResource(resources, R.drawable.baseline_tag_faces_white_48)))

        wheelItems.add(WheelItem(Color.GRAY, BitmapFactory.decodeResource(resources, R.drawable.baseline_tag_faces_white_48)))

        wheel.addWheelItems(wheelItems)

        lucky.setOnClickListener {
            wheel.rotateWheelTo(0)
        }

        wheel.setLuckyWheelReachTheTarget {
            Toast.makeText(this@WheelActivity, "Congrats!", Toast.LENGTH_SHORT).show()
        }
    }
}
