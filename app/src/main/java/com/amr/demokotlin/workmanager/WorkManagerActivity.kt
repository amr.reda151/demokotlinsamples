package com.amr.demokotlin.workmanager

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.work.*
import com.amr.demokotlin.R
import java.util.concurrent.TimeUnit

class WorkManagerActivity : AppCompatActivity() {

    @SuppressLint("RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_work_manager)

//        val toastWork = OneTimeWorkRequestBuilder<ToastWork>()
//            .setInitialDelay(5, TimeUnit.SECONDS)
//            .build()

        val toastWork = PeriodicWorkRequestBuilder<ToastWork>(
            2, TimeUnit.SECONDS
        ).setScheduleRequestedAt(5, TimeUnit.SECONDS)
            .build()

        WorkManager.getInstance().enqueue(toastWork)
    }
}
