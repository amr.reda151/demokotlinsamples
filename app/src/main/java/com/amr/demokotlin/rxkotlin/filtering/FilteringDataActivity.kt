package com.amr.demokotlin.rxkotlin.filtering

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.amr.demokotlin.R
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.rxkotlin.toObservable

class FilteringDataActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filtering_data)

        val list = listOf("Alpha", "Beta", "Gamma", "Delta", "Epsilon")

        list.toObservable() // extension function for Iterables
            .filter { it.equals("Alpha") }
            .subscribeBy(
                onNext = {
                    // here we can handle with the filtered data by, i.e. 'it'
                    Log.e("rxkotlinData", it)
                },
                onError = { it.printStackTrace() },
                onComplete = { Log.e("rxkotlinRESULT", "DONE!") }
            )

    }
}
