package com.amr.demokotlin.mvvm.retrofit.models

import com.amr.demokotlin.mvvm.retrofit.utilities.EndPoints
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET

interface Api {

    @GET(EndPoints.ALL_HALLS)
    fun getResponseForCoroutinesAsync(): Deferred<Response<ResponseObject>>

    @GET(EndPoints.ALL_HALLS)
    fun getResponse(): Call<ResponseObject>
}