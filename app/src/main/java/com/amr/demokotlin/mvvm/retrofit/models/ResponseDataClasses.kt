package com.amr.demokotlin.mvvm.retrofit.models

data class ResponseObject(var status: Int, var halls: Halls)

data class Halls(var current_page: Int, var data: List<Hall>)

data class Hall(var id: Int, var package_special_end_date: String)