package com.amr.demokotlin.mvvm.room.models

//@Database(entities = arrayOf(Human::class), version = 1)
//abstract class HumanDatabase : RoomDatabase() {
//    abstract fun humanDao(): HumanDao
//
//    class MyRoomCallback : RoomDatabase.Callback() {
//        override fun onCreate(db: SupportSQLiteDatabase) {
//            super.onCreate(db)
//
//            InsertDemoAsyncTask(INSTANCE)
//                .execute()
//        }
//    }
//
//    companion object {
//        private var INSTANCE: HumanDatabase? = null
//
//        var myRoomCallback = MyRoomCallback()
//
//        fun getInstance(context: Context): HumanDatabase? {
//
//            if (INSTANCE == null) {
//                synchronized(HumanDatabase::class) {
//                    INSTANCE = Room.databaseBuilder(context.applicationContext, HumanDatabase::class.java, "humans.db").fallbackToDestructiveMigration().addCallback(
//                        myRoomCallback
//                    ).build()
//                }
//            }
//
//            return INSTANCE
//        }
//    }
//
//    class InsertDemoAsyncTask(humanDb: HumanDatabase?) : AsyncTask<Unit, Unit, Unit>() {
//
//        private var humanDao: HumanDao? = null
//
//        init {
//            humanDao = humanDb?.humanDao()
//        }
//
//        override fun doInBackground(vararg params: Unit?) {
//            humanDao?.insert(Human(name = "Amr", age = 24))
//        }
//
//    }
//
//    fun destroyInstance() {
//        INSTANCE = null
//    }
//}