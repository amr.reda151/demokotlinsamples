package com.amr.demokotlin.mvvm.retrofit.utilities

class EndPoints {
    companion object {
        const val BASE_URL: String = "https://farrah.dev.ibtdi.work/api/"

        const val ALL_HALLS: String = "getnewhalls/1"
    }
}