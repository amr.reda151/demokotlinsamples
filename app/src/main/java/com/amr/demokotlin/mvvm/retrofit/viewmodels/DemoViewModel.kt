package com.amr.demokotlin.mvvm.retrofit.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.amr.demokotlin.mvvm.retrofit.models.Hall
import com.amr.demokotlin.mvvm.retrofit.models.Halls
import com.amr.demokotlin.mvvm.retrofit.models.ResponseModel

class DemoViewModel(application: Application) : AndroidViewModel(application), ResponseModel.PassingDataToViewModel {

    private var halls: MutableLiveData<List<Hall>> = MutableLiveData()
    private var connection: ResponseModel? = null

    init {
        connection = ResponseModel(this)
    }

    fun getHallsFromConnection() {
        connection!!.getData()
    }

    fun getHalls() : MutableLiveData<List<Hall>> {
        return halls
    }

    override fun passDataToVM(data: List<Hall>) {
        this.halls.value = data
    }
}