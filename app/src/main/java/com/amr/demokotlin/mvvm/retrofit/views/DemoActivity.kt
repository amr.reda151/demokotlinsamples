package com.amr.demokotlin.mvvm.retrofit.views

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.amr.demokotlin.R
import com.amr.demokotlin.mvvm.retrofit.models.Api
import com.amr.demokotlin.mvvm.retrofit.models.ResponseModel
import com.amr.demokotlin.mvvm.retrofit.viewmodels.DemoViewModel
import kotlinx.coroutines.*

class DemoActivity : AppCompatActivity() {
    private var viewModel: DemoViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_demo)

        /**
         * retrofit only
         * **/
//        viewModel = ViewModelProviders.of(this).get(DemoViewModel::class.java)
//
//        viewModel?.getHalls()?.observeForever {
//            for (h in it) {
//                Log.e("hallsDate", h.id.toString())
//            }
//        }
//
//        viewModel?.getHallsFromConnection()


        /**
         * retrofit with coroutines
         * **/
//        GlobalScope.launch {
//            val halls = getData()
//
//            Log.e("success", halls.toString())
//
////            halls.forEach {
////                Log.e("id", it.id.toString())
////            }
//        }

        CoroutineScope(Dispatchers.IO).launch {

            val halls = getData()

            withContext(Dispatchers.Main) {
                Log.e("boolean", halls.toString())
            }
        }
    }

    suspend fun getData(): Boolean {
        val retrofit = ResponseModel.getRetrofitClient()

        val api = retrofit!!.create(Api::class.java)
        val response = api.getResponseForCoroutinesAsync().await()

        return response.isSuccessful

    }
}
