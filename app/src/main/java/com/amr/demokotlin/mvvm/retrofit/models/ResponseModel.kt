package com.amr.demokotlin.mvvm.retrofit.models

import android.util.Log
import com.amr.demokotlin.mvvm.retrofit.utilities.EndPoints
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ResponseModel(val passingDataToViewModel: PassingDataToViewModel) {

    companion object {
        private var retrofit: Retrofit? = null

        fun getRetrofitClient(): Retrofit? {
            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                    .baseUrl(EndPoints.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(CoroutineCallAdapterFactory())
                    .build()
            }

            return retrofit
        }
    }

    class CallBackContent(val passingDataToViewModel: PassingDataToViewModel) : Callback<ResponseObject> {

        override fun onResponse(call: Call<ResponseObject>, response: Response<ResponseObject>) {
            val responseModel: ResponseObject = response.body()!!

            passingDataToViewModel.passDataToVM(responseModel.halls.data)
        }

        override fun onFailure(call: Call<ResponseObject>, t: Throwable) {
            Log.e("error", t.message)
        }

    }

    fun getData() {
        val api: Api = getRetrofitClient()!!.create(
            Api::class.java
        )

        val call: Call<ResponseObject> = api.getResponse()

        val callbackContent = object : Callback<ResponseObject> {
            override fun onResponse(call: Call<ResponseObject>, response: Response<ResponseObject>) {

                val responseModel: ResponseObject = response.body()!!

                passingDataToViewModel.passDataToVM(responseModel.halls.data)
            }

            override fun onFailure(call: Call<ResponseObject>, t: Throwable) {
                Log.e("error", t.message)
            }
        }

        call.enqueue(callbackContent)
    }

    interface PassingDataToViewModel {
        fun passDataToVM(data: List<Hall>)
    }

}