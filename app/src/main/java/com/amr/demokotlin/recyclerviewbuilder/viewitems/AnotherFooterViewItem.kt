package com.amr.demokotlin.recyclerviewbuilder.viewitems

import android.view.View
import com.amr.demokotlin.R
import com.amr.demokotlin.recyclerviewbuilder.builder.ViewItem
import com.amr.demokotlin.recyclerviewbuilder.builder.ViewItemRepresentable
import com.amr.demokotlin.recyclerviewbuilder.models.AnotherPost
import com.amr.demokotlin.recyclerviewbuilder.models.Post

class AnotherFooterViewItem: ViewItem<ViewItemRepresentable>(R.layout.item_another_footer) {
    override fun bind(itemView: View, viewItemPosition: Int) {}
}