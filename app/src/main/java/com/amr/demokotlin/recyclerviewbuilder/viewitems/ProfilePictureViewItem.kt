package com.amr.demokotlin.recyclerviewbuilder.viewitems

import android.util.Log
import android.view.View
import com.amr.demokotlin.R
import com.amr.demokotlin.recyclerviewbuilder.builder.ViewItem
import com.amr.demokotlin.recyclerviewbuilder.builder.ViewItemRepresentable
import com.amr.demokotlin.recyclerviewbuilder.models.ProfilePicture
import com.amr.demokotlin.utilities.extensions.imageview.load
import kotlinx.android.synthetic.main.item_recycler_builder_3.view.*

class ProfilePictureViewItem(
    private val model: ProfilePicture
) : ViewItem<ViewItemRepresentable>(R.layout.item_recycler_builder_3, model) {
    override fun bind(itemView: View, viewItemPosition: Int) {
        Log.e("positions", viewItemPosition.toString())

        itemView.userName.text = model.user.name
        itemView.time.text = model.time

        itemView.img.load(model.profilePicture)
    }
}