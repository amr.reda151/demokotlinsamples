package com.amr.demokotlin.recyclerviewbuilder.builder

data class ViewItemsObserver(
    var viewItemsArrayList: ViewItemArrayList = arrayListOf(),
    var clearsOnSet: Boolean = false,
    var appendToEnd: Boolean = true
)