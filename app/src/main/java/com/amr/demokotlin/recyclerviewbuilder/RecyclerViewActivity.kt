package com.amr.demokotlin.recyclerviewbuilder

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import com.amr.demokotlin.R
import com.amr.demokotlin.recyclerviewbuilder.builder.RecyclerViewBuilder
import com.amr.demokotlin.recyclerviewbuilder.builder.RecyclerViewBuilderFactory
import com.amr.demokotlin.recyclerviewbuilder.builder.ViewItemRepresentable
import com.amr.demokotlin.recyclerviewbuilder.builder.ViewItemsObserver
import com.amr.demokotlin.recyclerviewbuilder.models.ImageAndContent
import com.amr.demokotlin.recyclerviewbuilder.models.Images
import com.amr.demokotlin.recyclerviewbuilder.models.ProfilePicture
import com.amr.demokotlin.recyclerviewbuilder.viewitems.FooterViewItem
import com.amr.demokotlin.utilities.extensions.arraylist.toArrayList
import kotlinx.android.synthetic.main.activity_recycler_view.*

class RecyclerViewActivity : AppCompatActivity() {

    private lateinit var recyclerViewBuilder: RecyclerViewBuilder
    private val viewItems = MutableLiveData<ViewItemsObserver>()

    private lateinit var models: ArrayList<ViewItemRepresentable>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycler_view)

        models = arrayListOf()
        models.add(ImageAndContent())
        models.add(Images())

        val viewItemsList = models.map { it.viewItem }.toArrayList()

        recyclerViewBuilder = RecyclerViewBuilderFactory(recyclerView)
            .buildWithLinearLayout()

        with(recyclerViewBuilder) {
            setLoadingView(loading)
            setEmptyView(noData)
            setFooter(FooterViewItem())
            startLoading()
            setViewItems(viewItemsList)
        }

        viewItems.value = ViewItemsObserver(viewItemsList, false)

        test.setOnClickListener {

            recyclerViewBuilder.removeViewItem(0)

        }
    }
}
