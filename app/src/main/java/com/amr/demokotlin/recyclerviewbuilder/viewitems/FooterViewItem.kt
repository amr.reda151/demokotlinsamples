package com.amr.demokotlin.recyclerviewbuilder.viewitems

import com.amr.demokotlin.R
import com.amr.demokotlin.recyclerviewbuilder.builder.ViewItem
import com.amr.demokotlin.recyclerviewbuilder.builder.ViewItemRepresentable

class FooterViewItem: ViewItem<ViewItemRepresentable>(R.layout.item_footer)