package com.amr.demokotlin.recyclerviewbuilder.models

import com.amr.demokotlin.R
import com.amr.demokotlin.recyclerviewbuilder.builder.ViewItem
import com.amr.demokotlin.recyclerviewbuilder.builder.ViewItemRepresentable
import com.amr.demokotlin.recyclerviewbuilder.viewitems.ProfilePictureViewItem

class ProfilePicture(
    var user: User = User("Cristian Bale"),
    var profilePicture: Int = R.drawable.avatar
) : ViewItemRepresentable, Post(time = "Mar 9 at 10:05 PM") {
    override val viewItem: ViewItem<ViewItemRepresentable>
        get() = ProfilePictureViewItem(this)
}