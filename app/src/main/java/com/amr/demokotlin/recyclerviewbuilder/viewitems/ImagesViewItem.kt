package com.amr.demokotlin.recyclerviewbuilder.viewitems

import android.util.Log
import android.view.View
import com.amr.demokotlin.R
import com.amr.demokotlin.recyclerviewbuilder.builder.ViewItem
import com.amr.demokotlin.recyclerviewbuilder.builder.ViewItemRepresentable
import com.amr.demokotlin.recyclerviewbuilder.models.Images
import com.amr.demokotlin.utilities.extensions.imageview.ImageViewMode
import com.amr.demokotlin.utilities.extensions.imageview.load
import kotlinx.android.synthetic.main.item_recycler_builder_2.view.*

class ImagesViewItem(
    private val model: Images
) : ViewItem<ViewItemRepresentable>(R.layout.item_recycler_builder_2, model) {
    override fun bind(itemView: View, viewItemPosition: Int) {
        Log.e("positions", viewItemPosition.toString())

        itemView.userName.text = model.user.name
        itemView.time.text = model.time

        itemView.img_1.load(model.images[0], ImageViewMode.CENTER_CROP)
        itemView.img_2.load(model.images[1], ImageViewMode.CENTER_CROP)
        itemView.img_3.load(model.images[2], ImageViewMode.CENTER_CROP)
        itemView.img_4.load(model.images[3], ImageViewMode.CENTER_CROP)
        itemView.img_5.load(model.images[4], ImageViewMode.CENTER_CROP)
    }
}