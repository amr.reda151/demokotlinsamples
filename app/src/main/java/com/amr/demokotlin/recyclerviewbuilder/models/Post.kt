package com.amr.demokotlin.recyclerviewbuilder.models

import com.amr.demokotlin.recyclerviewbuilder.builder.AbstractViewItem
import com.amr.demokotlin.recyclerviewbuilder.builder.ViewItemRepresentable
import com.amr.demokotlin.recyclerviewbuilder.viewitems.FooterViewItem

open class Post(var content: String = "Hello", var time: String = "5:15")
//    : ViewItemRepresentable {
//    override val viewItem: AbstractViewItem<ViewItemRepresentable>
//        get() = FooterViewItem(this)
//}