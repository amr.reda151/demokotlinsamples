package com.amr.demokotlin.recyclerviewbuilder.viewitems

import com.amr.demokotlin.R
//import com.amr.demokotlin.databinding.ItemPostBinding
import com.amr.demokotlin.recyclerviewbuilder.builder.BindingViewItem
import com.amr.demokotlin.recyclerviewbuilder.builder.ViewItemRepresentable
import com.amr.demokotlin.recyclerviewbuilder.models.Post

//class PostViewItem(
//    private val model: Post
//) : BindingViewItem<ViewItemRepresentable, ItemPostBinding>(R.layout.item_post, model) {
//    override fun bind(binding: ItemPostBinding, viewItemPosition: Int) {
//        binding.model = model
//    }
//}