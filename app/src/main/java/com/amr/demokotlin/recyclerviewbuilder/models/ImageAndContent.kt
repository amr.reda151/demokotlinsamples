package com.amr.demokotlin.recyclerviewbuilder.models

import com.amr.demokotlin.R
import com.amr.demokotlin.recyclerviewbuilder.builder.ViewItem
import com.amr.demokotlin.recyclerviewbuilder.builder.ViewItemRepresentable
import com.amr.demokotlin.recyclerviewbuilder.viewitems.ImageAndContentViewItem

class ImageAndContent(
    var user: User = User("Robert Downey"),
    val images: ArrayList<Int> = arrayListOf(R.drawable.img_3)
) : ViewItemRepresentable, Post("Van Gogh, Died: 29 July 1890", "Mar 9 at 8:15 PM") {
    override val viewItem: ViewItem<ViewItemRepresentable>
        get() = ImageAndContentViewItem(this)
}