package com.amr.demokotlin.recyclerviewbuilder.builder

interface ViewItemRepresentable {
    val viewItem: AbstractViewItem<ViewItemRepresentable>
}