package com.amr.demokotlin.recyclerviewbuilder.models

import com.amr.demokotlin.R
import com.amr.demokotlin.recyclerviewbuilder.builder.ViewItem
import com.amr.demokotlin.recyclerviewbuilder.builder.ViewItemRepresentable
import com.amr.demokotlin.recyclerviewbuilder.viewitems.ImagesViewItem

class Images(
    var user: User = User("Ben Affleck"),
    var images: ArrayList<Int> = arrayListOf(
        R.drawable.img_1,
        R.drawable.img_2,
        R.drawable.img_3,
        R.drawable.img_4,
        R.drawable.img_6
    )
) : ViewItemRepresentable, Post(time = "Mar 9 at 9:10 PM") {
    override val viewItem: ViewItem<ViewItemRepresentable>
        get() = ImagesViewItem(this)
}