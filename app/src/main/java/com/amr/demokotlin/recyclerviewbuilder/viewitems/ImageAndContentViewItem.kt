package com.amr.demokotlin.recyclerviewbuilder.viewitems

import android.util.Log
import android.view.View
import com.amr.demokotlin.R
import com.amr.demokotlin.recyclerviewbuilder.builder.ViewItem
import com.amr.demokotlin.recyclerviewbuilder.builder.ViewItemRepresentable
import com.amr.demokotlin.recyclerviewbuilder.models.ImageAndContent
import com.amr.demokotlin.utilities.extensions.imageview.ImageViewMode
import com.amr.demokotlin.utilities.extensions.imageview.load
import kotlinx.android.synthetic.main.item_recycler_builder_1.view.*

class ImageAndContentViewItem(
    private val model: ImageAndContent
) : ViewItem<ViewItemRepresentable>(R.layout.item_recycler_builder_1, model) {
    override fun bind(itemView: View, viewItemPosition: Int) {
        Log.e("positions", viewItemPosition.toString())
        itemView.userName.text = model.user.name
        itemView.time.text = model.time
        itemView.content.text = model.content
        itemView.img.load(model.images[0], ImageViewMode.CENTER_CROP)
    }
}