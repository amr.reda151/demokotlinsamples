package com.amr.demokotlin.dagger

import android.app.Application

class MyApplication: Application() {

    lateinit var component: ApplicationComponent
    private set

    override fun onCreate() {
        super.onCreate()
        component = DaggerApplicationComponent.builder()
            .contextModule(ContextModule(this))
            .build()
    }
}