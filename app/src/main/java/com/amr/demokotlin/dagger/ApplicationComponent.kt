package com.amr.demokotlin.dagger

import com.amr.demokotlin.mvp.view.MainActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ContextModule::class])
interface ApplicationComponent {
    fun inject(target: InjectionActivity)
}