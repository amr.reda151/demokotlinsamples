package com.amr.demokotlin.dagger

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.amr.demokotlin.R
import javax.inject.Inject

class InjectionActivity : AppCompatActivity() {

    @Inject
    lateinit var testContext: Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_injection)

        (application as MyApplication).component.inject(this)
        Toast.makeText(testContext,"Hello",Toast.LENGTH_SHORT).show()
    }
}
